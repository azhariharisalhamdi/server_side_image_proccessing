import cv2
import sys
import os, errno

'''
	it will be generate croped face from ID in /repo/datatest/$username
'''

temp_video = cv2.VideoCapture(sys.argv[2])
file_name = os.path.basename(sys.argv[2])
username = sys.argv[1]

name = os.path.splitext(file_name)[0]
success,image = temp_video.read()
count = 0
success = True

#resultDir = r'../user/'+username+'/datatest' 
resultDir = r'../datatest/'+username 
try:
    os.makedirs(resultDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        print ("file exist")
        raise
'''
if not os.path.exists(resultDir):
    os.makedirs(resultDir)
'''
#os.chdir('../user/'+username+'/datatest' )
os.chdir('../datatest/'+username )
try:
	while success:
		success,image = temp_video.read()
		if success==True :
			print ("Generate a new frame: %d" % count)
			cv2.imwrite(username+"_extract_frame_%d.jpg" % count, image)
			count += 1
		else:
			print("frame extraction finished")

except cv2.error as e:
	print("fail")
	print(e)
