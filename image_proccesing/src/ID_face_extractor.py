import numpy as np
import cv2
import sys
import os, errno

'''
	it will be generate croped face from ID in /repo/datatest/$username
'''

dataFace = cv2.CascadeClassifier('../dataset/face/haarcascade_frontalface_alt.xml')
username = file_name = sys.argv[1]
image = temp_image = cv2.imread(sys.argv[2])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#resultDir = r'../user/'+username+'/datatest' 
resultDir = r'../datatest/'+username 
try:
    os.makedirs(resultDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        print ("file exist")
        raise
'''
if not os.path.exists(resultDir):
    os.makedirs(resultDir)
'''

#cv2.imshow('detected image',temp_image)
#temp_file_name = os.path.splitext(sys.argv[2])[0]
#file_name = os.path.basename(sys.argv[2])

#os.chdir('../user/'+username+'/datatest' )
os.chdir('../datatest/'+username )

try:
	faces = dataFace.detectMultiScale(gray, 1.1, 5)
	for (x,y,w,h) in faces:
		cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
		temp_image = image
		crop= image[ y:h+y,x:w+x]
    		
	cv2.imwrite(username+'_face_ID_detection.jpg', temp_image)
	cv2.imwrite(username+'_face_ID_result.jpg',crop)
	
	print("face extraction finished")
	
except cv2.error as e:
	print("fail")
	print(e)
